import { createWebHistory, createRouter } from "vue-router";
import InicioPaises from "@/components/paises/InicioPaises";
import MostrarPaises from "@/components/paises/MostrarPaises";
import InsertarPaises from "@/components/paises/InsertarPaises";
import ActualizarPaises from "@/components/paises/ActualizarPaises";
import InicioEstadios from "@/components/estadios/InicioEstadios";
import MostrarEstadios from "@/components/estadios/MostrarEstadios";
import InsertarEstadios from "@/components/estadios/InsertarEstadios";
import ActualizarEstadios from "@/components/estadios/ActualizarEstadios";
import InicioEquipos from "@/components/equipos/InicioEquipos";
import MostrarEquipos from "@/components/equipos/MostrarEquipos";
import InsertarEquipos from "@/components/equipos/InsertarEquipos";
import ActualizarEquipos from "@/components/equipos/ActualizarEquipos";

const routes = [
    {
        path: "/",
        name: "InicioPaises",
        component: InicioPaises
    },
    {
        path: "/paises/crear",
        name: "InsertarPaises",
        component: InsertarPaises
    },
    {
        path: "/paises/:id/actualizar",
        name: "ActualizarPaises",
        component: ActualizarPaises
    },
    {
        path: "/paises/:id",
        name: "MostrarPaises",
        component: MostrarPaises
    },
    {
        path: "/estadios",
        name: "InicioEstadios",
        component: InicioEstadios
    },
    {
        path: "/estadios/crear",
        name: "InsertarEstadios",
        component: InsertarEstadios
    },
    {
        path: "/estadios/:id/actualizar",
        name: "ActualizarEstadios",
        component: ActualizarEstadios
    },
    {
        path: "/estadios/:id",
        name: "MostrarEstadios",
        component: MostrarEstadios
    },
    {
        path: "/equipos",
        name: "InicioEquipos",
        component: InicioEquipos
    },
    {
        path: "/equipos/crear",
        name: "InsertarEquipos",
        component: InsertarEquipos
    },
    {
        path: "/equipos/:id/actualizar",
        name: "ActualizarEquipos",
        component: ActualizarEquipos
    },
    {
        path: "/equipos/:id",
        name: "MostrarEquipos",
        component: MostrarEquipos
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;